package com.sdacademy.controller.questionnaire;

import com.sdacademy.controller.user.UserDTO;
import com.sdacademy.domain.AnswerToQuestionnaire;
import com.sdacademy.domain.Question;
import com.sdacademy.domain.Questionnaire;
import lombok.Data;


import java.util.LinkedList;
import java.util.List;


@Data
public class QuestionnaireDTO {
    private Long id;
    private String title;
    private String description;
    private UserDTO user;
    private List<QuestionDTO> questions;
    private List<AnswerToQuestionnaireDTO> answerToQuestionnaires;

    public QuestionnaireDTO(){}

    public Questionnaire gQuestionnaire() {
        Questionnaire questionnaire = new Questionnaire();
     //   questionnaire.setId(id);
        questionnaire.setTitle(title);
        questionnaire.setDescription(description);
        if (user != null) questionnaire.setUser(user.gUser());

        List<Question> questions = new LinkedList<>();
        if (this.questions!=null) {
            for(QuestionDTO dto : this.questions) {
                questions.add(dto.gQuestion());
            }
            questionnaire.setQuestions(questions);
        }


        List<AnswerToQuestionnaire> answerToQuestionnaires = new LinkedList<>();
        if (this.answerToQuestionnaires!=null) {
            for(AnswerToQuestionnaireDTO dto : this.answerToQuestionnaires) {
                answerToQuestionnaires.add(dto.gAnswerToQuestionnaire());
            }
            questionnaire.setAnswerToQuestionnaires(answerToQuestionnaires);
        }



        return questionnaire;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("QuestionnaireDTO{");
        sb.append("id=").append(id);
        sb.append(", title='").append(title).append('\'');
        sb.append(", description='").append(description).append('\'');
//        sb.append(", user=").append(user);
//        sb.append(", questions=").append(questions);
//        sb.append(", answerToQuestionnaires=").append(answerToQuestionnaires);
        sb.append('}');
        return sb.toString();
    }

}


