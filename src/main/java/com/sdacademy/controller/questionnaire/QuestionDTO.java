package com.sdacademy.controller.questionnaire;

import com.sdacademy.domain.Option;
import com.sdacademy.domain.Question;
import lombok.Data;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by greatidea on 2016-09-06.
 */
@Data
public class QuestionDTO {
    private Long id;
    private String contents;
    private List<OptionDTO> options;

    public QuestionDTO(){}

    public Question gQuestion() {
        Question question = new Question();
        //question.setId(id);
        question.setContents(contents);

        List<Option> options = new LinkedList<>();
        if (this.options!=null){
            for(OptionDTO dto : this.options) {
                options.add(dto.gOption());
            }
        question.setOptions(options);
        }

        return question;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public List<OptionDTO> getOptions() {
        return options;
    }

    public void setOptions(List<OptionDTO> options) {
        this.options = options;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("QuestionDTO{");
        sb.append("id=").append(id);
        sb.append(", contents='").append(contents).append('\'');
        sb.append(", options=").append(options);
        sb.append('}');
        return sb.toString();
    }
}
