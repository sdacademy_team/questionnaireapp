package com.sdacademy.controller.questionnaire;

import com.sdacademy.domain.Option;
import lombok.Data;

/**
 * Created by greatidea on 2016-09-06.
 */
@Data
public class OptionDTO {
    private Long id;
    private String contents;
    private AnswerDTO answer;

    public OptionDTO(){}

    public Option gOption(){
        Option option = new Option();
        //option.setId(id);
        option.setContents(contents);
        if (answer != null) option.setAnswer(answer.gAnswer());
        return option;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContents() {
        return contents;
    }

    public void setContents(String contents) {
        this.contents = contents;
    }

    public AnswerDTO getAnswer() {
        return answer;
    }

    public void setAnswer(AnswerDTO answer) {
        this.answer = answer;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("OptionDTO{");
        sb.append("id=").append(id);
        sb.append(", contents='").append(contents).append('\'');
        sb.append(", answer=").append(answer);
        sb.append('}');
        return sb.toString();
    }
}
