package com.sdacademy.controller.questionnaire;

import com.sdacademy.domain.Questionnaire;
import com.sdacademy.repository.QuestionnaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by greatidea on 2016-09-05.
 */


@RestController
@RequestMapping("/questionnaire")
public class QuestionnaireController {

    @Autowired
    private QuestionnaireRepository questionnaireRepository;



    @RequestMapping(value = "/add",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void addQuestionnaire(@RequestBody QuestionnaireDTO questionnaire, HttpServletResponse response) {
        System.out.println("---------> " + questionnaire.toString());

        questionnaireRepository.save( questionnaire.gQuestionnaire());
        Questionnaire q = questionnaire.gQuestionnaire();

        System.out.println("---------> Jest ok " + questionnaire.gQuestionnaire());
    }


    /*@ResponseBody
    public void addQuestionnaire(@RequestBody QuestionnaireDTO questionnaire) {
        System.out.println("---------> " + questionnaire.toString());
        questionnaireRepository.saveAndFlush( questionnaire.gQuestionnaire());
        System.out.println("---------> Jest ok");
    }*/


/*

    @RequestMapping(value = "/getuser",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User getUser(String mail) {
        System.out.println("---------> " + mail);
        User user = userRepository.findOneByMail(mail);
        System.out.println("---------> Jest ok" + user.toString());
        return user;
    }
* */

}
