package com.sdacademy.controller.questionnaire;

import com.sdacademy.domain.Answer;
import lombok.Data;

@Data
public class AnswerDTO {
    private Long id;
    private QuestionDTO question;
    private OptionDTO option;
    private AnswerToQuestionnaireDTO answerToQuestionnaire;

    public AnswerDTO(){}

    public Answer gAnswer(){
        Answer answer = new Answer();
        //answer.setId(id);
        answer.setQuestion(question.gQuestion());
        answer.setOption(option.gOption());
        answer.setAnswerToQuestionnaire(answerToQuestionnaire.gAnswerToQuestionnaire());

        return answer;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QuestionDTO getQuestion() {
        return question;
    }

    public void setQuestion(QuestionDTO question) {
        this.question = question;
    }

    public OptionDTO getOption() {
        return option;
    }

    public void setOption(OptionDTO option) {
        this.option = option;
    }

    public AnswerToQuestionnaireDTO getAnswerToQuestionnaire() {
        return answerToQuestionnaire;
    }

    public void setAnswerToQuestionnaire(AnswerToQuestionnaireDTO answerToQuestionnaire) {
        this.answerToQuestionnaire = answerToQuestionnaire;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AnswerDTO{");
        sb.append("id=").append(id);
        sb.append(", question=").append(question);
        sb.append(", option=").append(option);
        sb.append(", answerToQuestionnaire=").append(answerToQuestionnaire);
        sb.append('}');
        return sb.toString();
    }
}
