package com.sdacademy.controller.questionnaire;

import com.sdacademy.controller.user.UserDTO;
import com.sdacademy.domain.AnswerToQuestionnaire;
import lombok.Data;

@Data
public class AnswerToQuestionnaireDTO {
    private Long id;
    private QuestionnaireDTO questionnaire;
    private UserDTO user;



    public AnswerToQuestionnaireDTO() {}

    public AnswerToQuestionnaire gAnswerToQuestionnaire(){
        AnswerToQuestionnaire answerToQuestionnaire = new AnswerToQuestionnaire();
        //answerToQuestionnaire.setId(id);
        answerToQuestionnaire.setQuestionnaire(questionnaire.gQuestionnaire());
        answerToQuestionnaire.setUser(user.gUser());

        return answerToQuestionnaire;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public QuestionnaireDTO getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(QuestionnaireDTO questionnaire) {
        this.questionnaire = questionnaire;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("AnswerToQuestionnaireDTO{");
        sb.append("id=").append(id);
        sb.append(", questionnaire=").append(questionnaire);
        sb.append(", user=").append(user);
        sb.append('}');
        return sb.toString();
    }
}

