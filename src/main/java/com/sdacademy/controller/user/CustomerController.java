package com.sdacademy.controller.user;

import com.sdacademy.domain.User;
import com.sdacademy.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

@RestController
    public class CustomerController {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping( value = "/settings/getCurrent", method = RequestMethod.GET)
    public User getCurrentCustomer(Principal principal) {
        final String currentUserLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findOneByMail(currentUserLogin);
    }

    @RequestMapping(value = "/register",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void register( @RequestBody UserDTO user) {
        System.out.println("---------> " + user.toString());
        userRepository.saveAndFlush(user.gUser());
        System.out.println("---------> Jest ok");
    }


    /*@RequestMapping("/user")
    public Principal user(Principal user) {
        return user;
    }*/


    @RequestMapping(value = "/getuser",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public User getUser(String mail) {
        System.out.println("---------> " + mail);
        User user = userRepository.findOneByMail(mail);
        System.out.println("---------> Jest ok" + user.toString());
        return user;
    }

    @RequestMapping(value = "/getallusers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<User> getAllUsers() {

        List<User> users = userRepository.findAll();
        System.out.println("---------> Jest ok,  There is "+ users.size() + " users.");
        return users;
    }
}


