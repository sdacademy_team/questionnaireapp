package com.sdacademy.controller.user;


import com.sdacademy.configuration.AuthoritiesConstants;
import com.sdacademy.domain.User;
import lombok.Data;


@Data
public class UserDTO {

    private String surname;
    private String name;
    private String email;
    private String password;
    private String login;

    public UserDTO(){}
    public User gUser(){
        User user = new User();
        user.setRole(AuthoritiesConstants.USER);
        user.setMail(email);
        user.setPassword(password);
        user.setName(name);
        user.setSurname(surname);
        user.setLogin(login);
        return user;
    }
//
//    public String getSurname() {
//        return surname;
//    }
//
//    public void setSurname(String surname) {
//        this.surname = surname;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getEmail() {
//        return email;
//    }
//
//    public void setEmail(String email) {
//        this.email = email;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    public String getLogin() {
//        return login;
//    }
//
//    public void setLogin(String login) {
//        this.login = login;
//    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("UserDTO{");
        sb.append("surname='").append(surname).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", email='").append(email).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", login='").append(login).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
