package com.sdacademy.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="Answer_ID")
    private long id;

    @OneToOne(mappedBy = "answer", cascade = CascadeType.PERSIST)
    private Option option;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="Question_ID")
    private Question question;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="AnswerToQuestionnaire")
    private AnswerToQuestionnaire AnswerToQuestionnaire;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Option getOption() {
        return option;
    }

    public void setOption(Option option) {
        this.option = option;
    }

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public com.sdacademy.domain.AnswerToQuestionnaire getAnswerToQuestionnaire() {
        return AnswerToQuestionnaire;
    }

    public void setAnswerToQuestionnaire(com.sdacademy.domain.AnswerToQuestionnaire answerToQuestionnaire) {
        AnswerToQuestionnaire = answerToQuestionnaire;
    }
}
