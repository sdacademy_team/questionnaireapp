package com.sdacademy.domain;

import javax.persistence.*;

@Entity
public class AnswerToQuestionnaire {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="AnswerToQuestionnaire_ID")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="User_ID")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name="Questionnaire_ID")
    private Questionnaire questionnaire;




    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Questionnaire getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(Questionnaire questionnaire) {
        this.questionnaire = questionnaire;
    }
}
