package com.sdacademy.domain;

import javax.persistence.*;
import java.util.List;

@Entity
public class Questionnaire {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="Questionnaire_ID")
    private Long id;

    private String title;

    private String description;


    @OneToMany(mappedBy = "questionnaire", cascade = CascadeType.PERSIST)
    private List<Question> question;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    @JoinColumn(name = "User_ID")
    private User user;

    @OneToMany(mappedBy = "questionnaire", cascade = CascadeType.PERSIST)
    private List<AnswerToQuestionnaire> answerToQuestionnaires;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Question> getQuestions() {
        return question;
    }

    public void setQuestions(List<Question> question) {
        this.question = question;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<AnswerToQuestionnaire> getAnswerToQuestionnaires() {
        return answerToQuestionnaires;
    }

    public void setAnswerToQuestionnaires(List<AnswerToQuestionnaire> answerToQuestionnaires) {
        this.answerToQuestionnaires = answerToQuestionnaires;
    }

    public List<Question> getQuestion() {
        return question;
    }

    public void setQuestion(List<Question> question) {
        this.question = question;
    }
}
