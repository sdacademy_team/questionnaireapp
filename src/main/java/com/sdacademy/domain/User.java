package com.sdacademy.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "t_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="User_ID")
    private Long id;

    private String name;

    private String surname;

    private String password;

    private String mail;

    private String login;

    private String role;

    @OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
    private List<Questionnaire> questionnaire;

    @OneToMany(mappedBy = "user", cascade = CascadeType.PERSIST)
    private List<AnswerToQuestionnaire> answerToQuestionnaire;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<Questionnaire> getQuestionnaire() {
        return questionnaire;
    }

    public void setQuestionnaire(List<Questionnaire> questionnaire) {
        this.questionnaire = questionnaire;
    }

    public List<AnswerToQuestionnaire> getAnswerToQuestionnaire() {
        return answerToQuestionnaire;
    }

    public void setAnswerToQuestionnaire(List<AnswerToQuestionnaire> answerToQuestionnaire) {
        this.answerToQuestionnaire = answerToQuestionnaire;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("User{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", surname='").append(surname).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", mail='").append(mail).append('\'');
        sb.append(", login='").append(login).append('\'');
        sb.append(", role='").append(role).append('\'');
        sb.append(", questionnaire=").append(questionnaire);
        sb.append(", answerToQuestionnaire=").append(answerToQuestionnaire);
        sb.append('}');
        return sb.toString();
    }
}
