package com.sdacademy.configuration;

import com.sdacademy.domain.User;
import com.sdacademy.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Authenticate a user from the database.
 */
@Component("userDetailsService")
public class DetailsService implements UserDetailsService {

    private final Logger log = LoggerFactory.getLogger(UserDetailsService.class);

    @Inject
    private UserRepository userRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(final String login) {
        log.debug("Authenticating {}", login);
        String lowercaseLogin = login.toLowerCase();
        User user =  userRepository.findOneByMail(lowercaseLogin);
        List<GrantedAuthority> grantedAuthorities = new ArrayList<>();

        GrantedAuthority authority= new SimpleGrantedAuthority(user.getRole());
        grantedAuthorities.add(authority);

        return new org.springframework.security.core.userdetails.User(lowercaseLogin, user.getPassword(),  grantedAuthorities);

    }
}
