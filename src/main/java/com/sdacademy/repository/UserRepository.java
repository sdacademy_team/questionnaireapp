package com.sdacademy.repository;

import com.sdacademy.domain.AnswerToQuestionnaire;
import com.sdacademy.domain.Questionnaire;
import com.sdacademy.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by greatidea on 2016-09-01.
 */
public interface UserRepository  extends JpaRepository<User, Long> {
    User findOneByMail(String mail);
    List<Questionnaire> findByQuestionnaire(Long id);
    AnswerToQuestionnaire findByAnswerToQuestionnaire_Id(Long id);
   // List<User> find

}
