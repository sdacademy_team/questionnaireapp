package com.sdacademy.repository;

import com.sdacademy.domain.Answer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by greatidea on 2016-09-01.
 */
public interface AnswerRepository extends JpaRepository<Answer, Long> {
}
