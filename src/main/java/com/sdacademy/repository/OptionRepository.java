package com.sdacademy.repository;

import com.sdacademy.domain.Answer;
import com.sdacademy.domain.Option;
import com.sdacademy.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by greatidea on 2016-09-01.
 */
public interface OptionRepository extends JpaRepository<Option, Long> {

}
