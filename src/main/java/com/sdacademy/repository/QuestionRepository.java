package com.sdacademy.repository;

import com.sdacademy.domain.Option;
import com.sdacademy.domain.Question;
import com.sdacademy.domain.Questionnaire;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by greatidea on 2016-09-01.
 */
public interface QuestionRepository extends JpaRepository<Question, Long> {

    Questionnaire findByQuestionnaire(Long id);
}
