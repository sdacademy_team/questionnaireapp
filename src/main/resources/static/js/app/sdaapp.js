var app = angular.module('sdaapp', ['ngRoute'])
    .config(function($routeProvider) {
        console.log("ngroute");
        $routeProvider.when('/', {
            templateUrl : 'home.html',
            controller : 'HomeController'
        }).when('/login', {
            templateUrl : 'login.html',
            controller : 'NavigationController'
        }).when('/registerr', {
            templateUrl : 'registerr.html',
            controller : 'RegistersController'
        }).when('/questionnaire/add', {
            templateUrl : 'addquestionarry.html',
            controller : 'QuestionnaireController'
        }).otherwise('/');
    })